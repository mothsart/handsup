#!/usr/bin/python3
import os, sys
from pathlib import Path
from pathlib import PurePath
import time
try:
    import PIL.Image
    import PIL.ImageTk
except:
    print('Il manque la bibliothèque Python Imaging Library (PIL). Installez les paquets python3-pil et python3-pil.imagetk, ou utilisez la commande pip: "pip install pillow"')
import random
from tkinter import font

from lib.config import Conf

#fontname       = "Arial"
fontname       = "DejaVu Sans"
fontsize       = "20"
fontsizeslide  = "30"
fontsize_init  = "110"
fontcolor_abs  = "darkred"
fontcolor_pres = "darkgreen"
bgcolor        = "lightgrey"
record         = 1  #enregistrer la liste des élèves

class Application(object):
    def __init__(self, conf):
        """Constructeur de la fenêtre principale"""
        self.conf = conf
        self.root =Tk()
        self.root.title('Appel PS 2018-2019')

        self.presents = []
        self.absents =  []

        self.font2 = font.Font(family=fontname, size=fontsize)

        self.slideshow()

        # Code des couleurs pour les valeurs de zéro à neuf :
        self.cc = ['black','brown','red','orange','yellow',
            'green','blue','purple','grey','white']
        self.root.bind("<F11>", self.toggle_fullscreen)
        self.root.bind("<Escape>", self.end_fullscreen)
        # Action au clic gauche sur le canevas
        self.root.bind("<Button-1>", self.mouseDown)
        self.root.bind("<Button1-Motion>", self.moveMask)
        self.state = False

        #self.root.overrideredirect(1)  #Enlever les décorations
        self.toggle_fullscreen()
        self.root.focus_set()          # prendre le focus
        self.root.mainloop()

    def mouseDown(self, event):
        print(event.x, event.y)
        self.x1=event.x
        self.y1=event.y
        #Trouver l'objet le plus proche
        self.selObject = self.can.find_closest(self.x1, self.y1)
        print(self.selObject[0])
        print(self.mask)

    def moveMask(self, event):
        if self.selObject[0] == self.mask : # Si l'objet sélectionné est le masque
            x2, y2 = event.x, event.y
            dx, dy = x2 -self.x1, y2 -self.y1
            self.can.move(self.mask, dx, dy)
            self.x1, self.y1 = x2, y2

    def toggle_fullscreen(self, event=None):
        self.state = not self.state  # Just toggling the boolean
        self.root.attributes("-fullscreen", self.state)
        return "break"

    def end_fullscreen(self, event=None):
        if self.state == False :
            self.root.quit()                    
        self.state = False
        self.root.attributes("-fullscreen", False)
        return "break"

    def slideshow(self):
        """Canevas avec Photo des eleves"""
        self.slide=0 #slide courant
        self.screen_w = self.root.winfo_screenwidth()
        self.screen_h = self.root.winfo_screenheight()
        print("Screen size ", self.screen_w, "x", self.screen_h)

        self.liste()
        self.listMask()
        
        print("Il y a", len(self.list_m), "masques.")

        self.can = Canvas(self.root, width=self.screen_w, height =self.screen_h, bg =bgcolor)
        self.can.grid(row =1, column =1, columnspan =1, pady =0, padx =0)

        #Placer l'image de l'élève sur le canevas
        self.img = PhotoImage(file =self.list_e[self.slide])
        self.photo = self.can.create_image(self.screen_w//2, self.screen_h//3, image =self.img)
        
        #Placer le masque sur l'image de l'élève
        self.imgmask = PhotoImage(file =self.list_m[self.slide%len(self.list_m)])
        self.mask = self.can.create_image(self.screen_w//2, self.screen_h//3, image =self.imgmask)

        self.imgpresent = PhotoImage(file = self.conf.icons + '/present.png')
        self.imgabsent  = PhotoImage(file = self.conf.icons + '/absent.png' )

        self.bouton1 = Button(self.can, image = self.imgpresent, bg=bgcolor, command =self.present)
        self.fen_bouton1 = self.can.create_window(self.screen_w//3, 3*self.screen_h//4, window =self.bouton1)

        self.bouton2 = Button(self.can, image = self.imgabsent, bg=bgcolor, command =self.absent)
        self.fen_bouton2 = self.can.create_window(2*self.screen_w//3, 3*self.screen_h//4, window =self.bouton2)

        # Nom de l'élève
        print("mesure:", self.font2.measure(self.list_e[self.slide]))
        eleve=os.path.basename(self.list_e[self.slide])[0:-4]
        self.nom_e=self.can.create_text(self.screen_w//2, self.screen_h//3+self.img.height()//2, anchor =N, text = eleve, font =(fontname, fontsizeslide, 'bold'), fill = "black")
        # initiale de l'élève
        initiale=os.path.basename(self.list_e[self.slide])[0]
        self.init_e=self.can.create_text(self.screen_w//2-self.img.width()//2-80, self.screen_h//3, anchor =E, text = initiale, font =(fontname, fontsize_init, 'bold'), fill = "black")

        self.btn_stop()
        self.btn_bwd()

    def btn_stop(self,pos="NE"):
        '''Afficher un bouton d'arrêt dans le canevas'''
        self.imgstop  = PhotoImage(file = self.conf.icons + '/stop.png' )
        self.bouton = Button(self.can, image = self.imgstop, bg=bgcolor, command =self.stop)
        if pos=="N":
            self.fen_bouton = self.can.create_window(self.screen_w/2, 30, window =self.bouton)
        else:
            self.fen_bouton = self.can.create_window(self.screen_w-30, 30, window =self.bouton)

    def btn_bwd(self,pos="NE"):
        '''Afficher un bouton retour dans le canevas'''
        self.imgbwd  = PhotoImage(file = self.conf.icons + '/retour.png' )
        self.bouton = Button(self.can, image = self.imgbwd, bg=bgcolor, command =self.backward)
        if pos=="N":
            self.fen_bouton = self.can.create_window(self.screen_w/2+60, 30, window =self.bouton)
        else:
            self.fen_bouton = self.can.create_window(self.screen_w-30-60, 30, window =self.bouton)

    def present(self):
        """Présent"""
        #self.can.config(height=300)
        #self.presents.append(os.path.basename(self.list_e[self.slide])[0:-4])
        self.presents.append(self.list_e[self.slide])
        self.slide+=1
        self.charge_slide(self.slide)

    def absent(self):
        """Absent"""
        #self.absents.append(os.path.basename(self.list_e[self.slide])[0:-4])
        self.absents.append(self.list_e[self.slide])
        self.slide+=1
        self.charge_slide(self.slide)

    def backward(self):
        if self.slide>0:
            self.slide-=1
            if self.list_e[self.slide] in self.absents:
                #del self.absents[self.list_e[self.slide]]
                self.absents.remove(self.list_e[self.slide])  # on enlève l'élève de la liste des absents
                print ("on supprime", self.list_e[self.slide], "des absents")
            elif self.list_e[self.slide] in self.presents:
                #del self.absents[self.list_e[self.slide]]
                self.presents.remove(self.list_e[self.slide])  # on enlève l'élève de la liste des présents
                print ("on supprime", self.list_e[self.slide], "des présents")
            else:
                print ("pas de présent ou absent à supprimer...")      
            self.charge_slide(self.slide)

    def record(self):
        """Enregistrer les présences"""
        date=str(time.localtime()[0])+'_'+str(time.localtime()[1])+'_'+str(time.localtime()[2])
        rapport="Liste des absents\n"
        file=open(self.conf.path + '/journal/'+date+'.txt','w')
        
        for eleve in self.absents:
            eleve=os.path.basename(eleve)[0:-4]
            rapport+=eleve+"\n"

        rapport+="\nListe des présents:\n"
        
        for eleve in self.presents:
            eleve=os.path.basename(eleve)[0:-4]
            rapport+=eleve+"\n"
        
        file.write(rapport)
        file.close()
        print(date)

    def charge_slide(self, slide):
        '''Charger un slide (photo d'élève)'''
        if(self.slide < self.nb_eleves):
            self.bouton1.config(state='disabled')
            self.bouton2.config(state='disabled')
            self.root.after(2000, self.show_buttons)
            # Changer la photo
            self.img = PhotoImage(file = self.list_e[slide])
            #self.photo.config(image =self.img)
            self.can.itemconfig(self.photo, image =self.img)
            # Changer le masque
            self.imgmask = PhotoImage(file =self.list_m[self.slide%len(self.list_m)])
            self.can.itemconfig(self.mask, image =self.imgmask)
            # Replacer le masque
            self.can.coords(self.mask, self.screen_w//2, self.screen_h//3)
            # Nom de l'élève
            eleve=os.path.basename(self.list_e[slide])[0:-4]
            self.can.itemconfig(self.nom_e, text=eleve)
            # initiale de l'élève
            initiale=os.path.basename(self.list_e[slide])[0]
            self.can.itemconfig(self.init_e, text=initiale)
            
#            self.timer(4)
#            self.bouton1.config(state='normal')
#            self.bouton2.config(state='normal')
        else :
            self.rapport()

    def show_buttons(self):
        self.bouton1.config(state='normal')
        self.bouton2.config(state='normal')        


    def rapport(self):
        self.can.delete(ALL) #vider le canevas
        self.btn_stop("N")
        i=0 # Numéro de ligne à afficher
        j=0 # Numéro de l'image courante
        #pas=(self.screen_h-40)//self.nb_eleves

        self.im_can={} #dico pour placer toutes les objets images

#        #Afficher les présents
#        if(len(self.presents)>0):
#            pas = self.screen_h//len(self.presents)
#            pas = min([pas, self.screen_h//4]) # Limiter la taille au quart de la hauteur de l'écran
#            size= pas, pas//1.05
#            for eleve in self.presents:
#                self.im = PIL.Image.open(eleve)
#                print("on charge", eleve)
#                self.im.thumbnail(size, PIL.Image.ANTIALIAS)
#                self.im_can[j] = PIL.ImageTk.PhotoImage(self.im)
#                eleve=os.path.basename(eleve)[0:-4]
#                self.can.create_image(pas//2, i*pas+pas//2, image = self.im_can[j])
#                
#                self.can.create_text(pas+5, i*pas+pas//2, anchor =W, text = eleve, font =(fontname, fontsize, 'bold'), fill =fontcolor_pres)
#                print("pas, pas//2", pas, pas//2)
#                i+=1
#                j+=1
#            i=0  #On repart à la première ligne

        #Afficher les absents
        if(len(self.absents)>0):
            pas_abs = self.screen_h//len(self.absents)
            pas_abs = min([pas_abs, self.screen_h//4]) # Limiter la taille au quart de la hauteur de l'écran
            size_abs= pas_abs, pas_abs//1.05
            for eleve in self.absents:
                #self.can.create_text(self.screen_w/4*3, 40+i*pas, anchor =E, text = eleve, font =('Times', 20, 'bold italic'), fill ='red')
                #i+=1

                self.im = PIL.Image.open(eleve)
                print("on charge", eleve)
                self.im.thumbnail(size_abs, PIL.Image.ANTIALIAS)
                self.im_can[j] = PIL.ImageTk.PhotoImage(self.im)
                eleve=os.path.basename(eleve)[0:-4]
                self.can.create_image(self.screen_w-pas_abs//2, i*pas_abs+pas_abs//2, image = self.im_can[j])
                
                self.can.create_text(self.screen_w-pas_abs-5, i*pas_abs+pas_abs//2, anchor =E, text = eleve, font =(fontname, fontsize, 'bold'), fill = fontcolor_abs)
                i+=1
                j+=1

    def timer(self, n):
        """fait appel à sleep sans interrompre les tâches en cours"""
        i=0
        while(i<n):
            i+=0.1
            time.sleep(0.1)


    def stop(self):
        print(self.presents)
        if record==1:
            self.record()
        self.root.quit()

    def liste(self):
        '''Lister les élèves d'après les images'''
        self.list_e=[]
        self.nb_eleves=0
        for p in  Path(self.conf.path + "/eleves").glob('./**/*'):
            if p.is_file():
                if str(p).lower()[-4:] == ".png" :
                    self.list_e.append(str(p))
                    self.nb_eleves+=1
        print ("Il y a", self.nb_eleves, "élèves")
        # mélanger la liste
        random.shuffle(self.list_e)
        
    def listMask(self):
        '''Lister les masques'''
        self.list_m=[]
        for p in Path(self.conf.path + "/mask").glob('./**/*'):
            if p.is_file():
                if str(p).lower()[-4:] == ".png" :
                    self.list_m.append(str(p))
        # mélanger la liste
        random.shuffle(self.list_m)

# Programme principal :
if __name__ == '__main__':
    from tkinter import *
    #from math import log10  # logarithmes en base 10
    f = Application(Conf())       # instanciation de l'objet application
