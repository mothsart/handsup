©lkiefer // lkiefer.org/blog

Le logiciel est livré sans aucune garantie de fonctionnement, en espérant qu'il puisse vous être utile. Vous pouvez l'utiliser, le modifier et le redistribuer comme bon vous semble.

Dépendances:
Python 3.5 ou supérieur
PythonTK : sous GNU/Linux il faut installer le paquet python3-tk 
Python Imaging Library (PIL). Installez les paquets python3-pil et python3-pil.imagetk, ou utilisez la commande pip: "pip install pillow"

Fonctionnement:
Pour utiliser le logiciel, il faut mettre les photos des élèves dans le dossier «eleves» au format png et avec leur prénom comme nom de fichier. La taille recommandée est de 300x300 pixels, l'image n'est pas redimensionnée. Vous pouvez utiliser ImageMagick pour convertir les fichiers facilement.

[Esc] pour sortir du plein écran et quitter le logiciel
[F11] Pour basculer entre mode fenêtre et plein écran

Lorsque tous les élèves sont passés, un fichier journal est créé contenant les absents et présents.

**appel_1.py**

Affichage des présents à gauche et absents à droite

**appel_2.py**

Affichage des absents à droite (cela permet de les compter)

**appel_3.py**

Image masquée: les enfants doivent s'aider de l'écriture de leur prénom et initiales. On peut déplacer le masque.

**appel_ecrit**

Les enfants sont autonomes pour se mettre en présent ou absent (preuve de concept, à améliorer pour une utilisation réelle)
