# Handsup

## Dev mode

launch 

```sh
./appel_1.py --dev
```

## Create a Debian package

```sh
git clone https://framagit.org/mothsart/handsup.git
cd handsup
dpkg-buildpackage -us -uc
```
