#!/usr/bin/python3
import os, sys
from pathlib import Path
from pathlib import PurePath
import time
try:
    import PIL.Image
    import PIL.ImageTk
except:
    print('Il manque la bibliothèque Python Imaging Library (PIL). Installez les paquets python3-pil et python3-pil.imagetk, ou utilisez la commande pip: "pip install pillow"')
import random
from math import ceil
from tkinter import *

from lib.config import Conf

#fontname       = "Arial"
fontname       = "DejaVu Sans"
fcolor         = "black"
fontsize       = "17"
color_abs      = "red"
color_pre      = "green"
bgcolor        = "lightgrey"

class Application(object):
    def __init__(self, conf):
        """Constructeur de la fenêtre principale"""
        self.conf = conf
        self.fen1 = Tk()
        self.fen1.title('Appel PS 2018-2019')
        self.liste()
        self.appel()

        # démarrage :
        self.fen1.mainloop()

    def appel(self):

        nb_e=len(self.list_e)
        print("Il y a", nb_e, "élèves")
        
        # Nous voulons 6 colonnes, on calcule le nombre de lignes
        nb_l=ceil(nb_e/6)

        #un nom pour chaque bouton
        self.bouton_e={}

        #eleve actif
        self.e_actif=''

        i=10
        col=1
        for eleve in self.list_e:
            if(i==nb_l+10):
                i=10
                col+=1
            nom_eleve=os.path.basename(eleve)[0:-4]
            self.bouton_e[eleve]=Button(self.fen1, text =nom_eleve, font=(fontname,fontsize,"bold"), bg=bgcolor, fg=fcolor, command= lambda key=eleve: self.changerImage(key))
            self.bouton_e[eleve].grid(row =2+i, column =col, padx=5, pady=5)
            i+=1

        self.btnabs = PhotoImage(file = self.conf.icons + '/absent2.png')
        self.btnpre = PhotoImage(file = self.conf.icons + '/present2.png')
        Button(self.fen1, text ='maison', bg=color_abs, image=self.btnabs, command=self.absent).grid( row =1, column =3)
        Button(self.fen1, text ='école' , bg=color_pre, image=self.btnpre, command=self.present).grid(row =1, column =4)

        # création d'un widget 'Canvas' contenant une image bitmap :
        self.can1 = Canvas(self.fen1, width =300, height =300, bg ='gray')
        self.photo = PhotoImage(file = self.conf.icons + '/eleve.png')
        self.item = self.can1.create_image(151, 151, image =self.photo)
        # Mise en page à l'aide de la méthode 'grid' :
        self.can1.grid(row =0, column =1, columnspan=6, padx =5, pady =5)
        
    def changerImage(self, eleve):
            # Changer la photo
            self.photo = PhotoImage(file = eleve)
            self.can1.itemconfig(self.item, image =self.photo)
            self.e_actif=eleve

    def present(self):
        if self.e_actif in self.list_e :
            self.bouton_e[self.e_actif].configure(bg=color_pre)
            self.photo = PhotoImage(file = self.conf.icons + "/eleve.png")
            self.can1.itemconfig(self.item, image =self.photo)
            self.e_actif=''
        else:
            print("L'élève actif n'est pas dans la liste")
    
    def absent(self):
        if self.e_actif in self.list_e :
            self.bouton_e[self.e_actif].configure(bg=color_abs)
            self.photo = PhotoImage(file = self.conf.icons + "/eleve.png")
            self.can1.itemconfig(self.item, image =self.photo)
            self.e_actif=''
        else:
            print("L'élève actif n'est pas dans la liste")
            
    def liste(self):
        '''Lister les élèves d'après les images'''
        self.list_e=[]
        for p in  Path(self.conf.path + "/eleves").glob('./**/*'):
            if p.is_file():
                if str(p).lower()[-4:] == ".png" :
                    self.list_e.append(str(p))
        # mélanger la liste
        random.shuffle(self.list_e)


# Programme principal :
if __name__ == '__main__':
    from tkinter import *
    #from math import log10  # logarithmes en base 10
    f = Application(Conf())       # instanciation de l'objet application

