import os
from os.path import dirname, isdir, join
import sys

class Conf:
    def __init__(self):
        """Initialize handsup config"""
        self.dev = False
        if len(sys.argv) > 1 and str(sys.argv[1]) == '--dev':
            self.dev = True
        self.home_path = "/home/%s/.config/handsup/" % os.environ["USER"]
        self.path = self.home_path
        if self.dev:
            self.path = dirname(dirname(__file__))
            self.icons = join(self.path, 'icons')
            return
        self.icons = '/usr/share/handsup/icons'
        if isdir(self.path):
            return
        self.path = '/usr/share/handsup'
